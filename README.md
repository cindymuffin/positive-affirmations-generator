# Positive Affirmations Generator

## About

"Affirmations are positive statements that can help you to challenge and overcome self-sabotaging and negative thoughts. When you repeat them often, and believe in them, you can start to make positive changes." - MindTools.com

This Positive Affirmations Generator is a minimalist web application that generates a positive affirmation quotes to help one overcome their challenges and negative thoughts.

## Installation

1. Git Fork and Clone repository
2. Navigate to `/positive-affirmations-generator` directory
3. Run the server

```
http-server
```

4. Access the application on http://localhost:8080
